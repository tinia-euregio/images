#!/usr/bin/env python3

# 1. start the smtp_sink server with:
#   docker run --rm -it -p 25:25 registry.gitlab.com/tinia-euregio/images/smtp_sink
# 2. run this script to send an email to the smtp_sink server
# 3. to retrieve the content of the email, run something like:
#   docker exec -it $(docker ps -q) cat /maildir/new/1734449670.M72222P7Q1.18784e1e9bbe

import datetime
from smtplib import SMTP

debuglevel = 0

smtp = SMTP()
smtp.set_debuglevel(debuglevel)
smtp.connect("localhost", 25)

from_addr = "Alice <alice@example.com>"
to_addr = "bob@example.com"

subj = "Test"
date = datetime.datetime.now().strftime("%d/%m/%Y %H:%M")

message_text = "Hello\nThis is Just a test.\n\nBye bye!\n"

msg = "From: %s\nTo: %s\nSubject: %s\nDate: %s\n\n%s" % (
    from_addr,
    to_addr,
    subj,
    date,
    message_text,
)

smtp.sendmail(from_addr, to_addr, msg)
smtp.quit()
