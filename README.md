# Build container images

Helper repo to build container images on Gitlab CI.

## How to test the image build locally

```sh
for IMAGE in lint_python rsync skopeo hugo nix smtp-sink logger
do
  docker build . -f "${I}/Dockerfile"
done
```

## License

Copyright (C) 2022 - 2025 [simevo s.r.l.](https://simevo.com) for [Europaregion Tirol/Südtirol/Trentino](https://www.europaregion.info).

Licensed under the [BSD 3-Clause (`BSD-3-Clause`)](LICENSE) as per [linee guida per l’Acquisizione e il riuso di software per la Pubblica Amministrazione](https://docs.italia.it/italia/developers-italia/lg-acquisizione-e-riuso-software-per-pa-docs/it/stabile/riuso-software/licenze-aperte-e-scelta-di-una-licenza.html#scelta-di-una-licenza).
