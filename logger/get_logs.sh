#!/bin/sh
scp -o "StrictHostKeyChecking=no" root@meteo.report:/var/log/nginx/access.log.1 /tmp/access.log.1
/usr/local/bin/python /opt/matomo/import_logs.py \
  --debug \
  --retry-max-attempts=1 \
  --url=https://matomo.simevo.com \
  --log-format-name=ncsa_extended \
  --idsite=1 \
  --token-auth="$MATOMO_TOKEN_AUTH" \
  /tmp/access.log.1
